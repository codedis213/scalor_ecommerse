# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class AccountAddress(models.Model):
    first_name = models.CharField(max_length=256)
    last_name = models.CharField(max_length=256)
    company_name = models.CharField(max_length=256)
    street_address_1 = models.CharField(max_length=256)
    street_address_2 = models.CharField(max_length=256)
    city = models.CharField(max_length=256)
    postal_code = models.CharField(max_length=20)
    country = models.CharField(max_length=2)
    country_area = models.CharField(max_length=128)
    phone = models.CharField(max_length=128)
    city_area = models.CharField(max_length=128)

    class Meta:
        managed = False
        db_table = 'account_address'


class AccountCustomerevent(models.Model):
    date = models.DateTimeField()
    type = models.CharField(max_length=255)
    parameters = django.contrib.postgres.fields.JSONField()
    order = models.ForeignKey('OrderOrder', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey('AccountUser', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'account_customerevent'


class AccountCustomernote(models.Model):
    date = models.DateTimeField()
    content = models.TextField()
    is_public = models.BooleanField()
    customer = models.ForeignKey('AccountUser', models.DO_NOTHING)
    user = models.ForeignKey('AccountUser', models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'account_customernote'


class AccountUser(models.Model):
    is_superuser = models.BooleanField()
    email = models.CharField(unique=True, max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    password = models.CharField(max_length=128)
    date_joined = models.DateTimeField()
    last_login = models.DateTimeField(blank=True, null=True)
    default_billing_address = models.ForeignKey(AccountAddress, models.DO_NOTHING, blank=True, null=True)
    default_shipping_address = models.ForeignKey(AccountAddress, models.DO_NOTHING, blank=True, null=True)
    note = models.TextField(blank=True, null=True)
    token = models.UUIDField(unique=True)
    first_name = models.CharField(max_length=256)
    last_name = models.CharField(max_length=256)
    avatar = models.CharField(max_length=100, blank=True, null=True)
    private_meta = django.contrib.postgres.fields.JSONField(blank=True, null=True)
    meta = django.contrib.postgres.fields.JSONField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'account_user'


class AccountUserAddresses(models.Model):
    user = models.ForeignKey(AccountUser, models.DO_NOTHING)
    address = models.ForeignKey(AccountAddress, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'account_user_addresses'
        unique_together = (('user', 'address'),)


class AccountUserGroups(models.Model):
    user = models.ForeignKey(AccountUser, models.DO_NOTHING)
    group = models.ForeignKey('AuthGroup', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'account_user_groups'
        unique_together = (('user', 'group'),)


class AccountUserUserPermissions(models.Model):
    user = models.ForeignKey(AccountUser, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'account_user_user_permissions'
        unique_together = (('user', 'permission'),)


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class CheckoutCheckout(models.Model):
    created = models.DateTimeField()
    last_change = models.DateTimeField()
    email = models.CharField(max_length=254)
    token = models.UUIDField(primary_key=True)
    quantity = models.IntegerField()
    user = models.ForeignKey(AccountUser, models.DO_NOTHING, blank=True, null=True)
    billing_address = models.ForeignKey(AccountAddress, models.DO_NOTHING, blank=True, null=True)
    discount_amount = models.DecimalField(max_digits=12, decimal_places=2)
    discount_name = models.CharField(max_length=255, blank=True, null=True)
    note = models.TextField()
    shipping_address = models.ForeignKey(AccountAddress, models.DO_NOTHING, blank=True, null=True)
    shipping_method = models.ForeignKey('ShippingShippingmethod', models.DO_NOTHING, blank=True, null=True)
    voucher_code = models.CharField(max_length=12, blank=True, null=True)
    translated_discount_name = models.CharField(max_length=255, blank=True, null=True)
    meta = django.contrib.postgres.fields.JSONField(blank=True, null=True)
    private_meta = django.contrib.postgres.fields.JSONField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'checkout_checkout'


class CheckoutCheckoutGiftCards(models.Model):
    checkout = models.ForeignKey(CheckoutCheckout, models.DO_NOTHING)
    giftcard = models.ForeignKey('GiftcardGiftcard', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'checkout_checkout_gift_cards'
        unique_together = (('checkout', 'giftcard'),)


class CheckoutCheckoutline(models.Model):
    quantity = models.IntegerField()
    checkout = models.ForeignKey(CheckoutCheckout, models.DO_NOTHING)
    variant = models.ForeignKey('ProductProductvariant', models.DO_NOTHING)
    data = django.contrib.postgres.fields.JSONField()

    class Meta:
        managed = False
        db_table = 'checkout_checkoutline'
        unique_together = (('checkout', 'variant', 'data'),)


class DiscountSale(models.Model):
    name = models.CharField(max_length=255)
    type = models.CharField(max_length=10)
    value = models.DecimalField(max_digits=12, decimal_places=2)
    end_date = models.DateTimeField(blank=True, null=True)
    start_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'discount_sale'


class DiscountSaleCategories(models.Model):
    sale = models.ForeignKey(DiscountSale, models.DO_NOTHING)
    category = models.ForeignKey('ProductCategory', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'discount_sale_categories'
        unique_together = (('sale', 'category'),)


class DiscountSaleCollections(models.Model):
    sale = models.ForeignKey(DiscountSale, models.DO_NOTHING)
    collection = models.ForeignKey('ProductCollection', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'discount_sale_collections'
        unique_together = (('sale', 'collection'),)


class DiscountSaleProducts(models.Model):
    sale = models.ForeignKey(DiscountSale, models.DO_NOTHING)
    product = models.ForeignKey('ProductProduct', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'discount_sale_products'
        unique_together = (('sale', 'product'),)


class DiscountSaletranslation(models.Model):
    language_code = models.CharField(max_length=10)
    name = models.CharField(max_length=255, blank=True, null=True)
    sale = models.ForeignKey(DiscountSale, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'discount_saletranslation'
        unique_together = (('language_code', 'sale'),)


class DiscountVoucher(models.Model):
    type = models.CharField(max_length=20)
    name = models.CharField(max_length=255, blank=True, null=True)
    code = models.CharField(unique=True, max_length=12)
    usage_limit = models.IntegerField(blank=True, null=True)
    used = models.IntegerField()
    start_date = models.DateTimeField()
    end_date = models.DateTimeField(blank=True, null=True)
    discount_value_type = models.CharField(max_length=10)
    discount_value = models.DecimalField(max_digits=12, decimal_places=2)
    min_amount_spent = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    apply_once_per_order = models.BooleanField()
    countries = models.CharField(max_length=749)
    min_checkout_items_quantity = models.IntegerField(blank=True, null=True)
    apply_once_per_customer = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'discount_voucher'


class DiscountVoucherCategories(models.Model):
    voucher = models.ForeignKey(DiscountVoucher, models.DO_NOTHING)
    category = models.ForeignKey('ProductCategory', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'discount_voucher_categories'
        unique_together = (('voucher', 'category'),)


class DiscountVoucherCollections(models.Model):
    voucher = models.ForeignKey(DiscountVoucher, models.DO_NOTHING)
    collection = models.ForeignKey('ProductCollection', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'discount_voucher_collections'
        unique_together = (('voucher', 'collection'),)


class DiscountVoucherProducts(models.Model):
    voucher = models.ForeignKey(DiscountVoucher, models.DO_NOTHING)
    product = models.ForeignKey('ProductProduct', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'discount_voucher_products'
        unique_together = (('voucher', 'product'),)


class DiscountVouchercustomer(models.Model):
    customer_email = models.CharField(max_length=254)
    voucher = models.ForeignKey(DiscountVoucher, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'discount_vouchercustomer'
        unique_together = (('voucher', 'customer_email'),)


class DiscountVouchertranslation(models.Model):
    language_code = models.CharField(max_length=10)
    name = models.CharField(max_length=255, blank=True, null=True)
    voucher = models.ForeignKey(DiscountVoucher, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'discount_vouchertranslation'
        unique_together = (('language_code', 'voucher'),)


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AccountUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoPricesOpenexchangeratesConversionrate(models.Model):
    to_currency = models.CharField(unique=True, max_length=3)
    rate = models.DecimalField(max_digits=20, decimal_places=12)
    modified_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_prices_openexchangerates_conversionrate'


class DjangoPricesVatlayerRatetypes(models.Model):
    types = models.TextField()

    class Meta:
        managed = False
        db_table = 'django_prices_vatlayer_ratetypes'


class DjangoPricesVatlayerVat(models.Model):
    country_code = models.CharField(max_length=2)
    data = models.TextField()

    class Meta:
        managed = False
        db_table = 'django_prices_vatlayer_vat'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class DjangoSite(models.Model):
    domain = models.CharField(unique=True, max_length=100)
    name = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'django_site'


class GiftcardGiftcard(models.Model):
    code = models.CharField(unique=True, max_length=16)
    created = models.DateTimeField()
    start_date = models.DateField()
    end_date = models.DateField(blank=True, null=True)
    last_used_on = models.DateTimeField(blank=True, null=True)
    is_active = models.BooleanField()
    initial_balance = models.DecimalField(max_digits=12, decimal_places=2)
    current_balance = models.DecimalField(max_digits=12, decimal_places=2)
    user = models.ForeignKey(AccountUser, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'giftcard_giftcard'


class ImpersonateImpersonationlog(models.Model):
    session_key = models.CharField(max_length=40)
    session_started_at = models.DateTimeField(blank=True, null=True)
    session_ended_at = models.DateTimeField(blank=True, null=True)
    impersonating = models.ForeignKey(AccountUser, models.DO_NOTHING)
    impersonator = models.ForeignKey(AccountUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'impersonate_impersonationlog'


class MenuMenu(models.Model):
    name = models.CharField(max_length=128)
    json_content = django.contrib.postgres.fields.JSONField()

    class Meta:
        managed = False
        db_table = 'menu_menu'


class MenuMenuitem(models.Model):
    name = models.CharField(max_length=128)
    sort_order = models.IntegerField(blank=True, null=True)
    url = models.CharField(max_length=256, blank=True, null=True)
    lft = models.IntegerField()
    rght = models.IntegerField()
    tree_id = models.IntegerField()
    level = models.IntegerField()
    category = models.ForeignKey('ProductCategory', models.DO_NOTHING, blank=True, null=True)
    collection = models.ForeignKey('ProductCollection', models.DO_NOTHING, blank=True, null=True)
    menu = models.ForeignKey(MenuMenu, models.DO_NOTHING)
    page = models.ForeignKey('PagePage', models.DO_NOTHING, blank=True, null=True)
    parent = models.ForeignKey('self', models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'menu_menuitem'


class MenuMenuitemtranslation(models.Model):
    language_code = models.CharField(max_length=10)
    name = models.CharField(max_length=128)
    menu_item = models.ForeignKey(MenuMenuitem, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'menu_menuitemtranslation'
        unique_together = (('language_code', 'menu_item'),)


class OrderFulfillment(models.Model):
    tracking_number = models.CharField(max_length=255)
    shipping_date = models.DateTimeField()
    order = models.ForeignKey('OrderOrder', models.DO_NOTHING)
    fulfillment_order = models.IntegerField()
    status = models.CharField(max_length=32)

    class Meta:
        managed = False
        db_table = 'order_fulfillment'


class OrderFulfillmentline(models.Model):
    order_line = models.ForeignKey('OrderOrderline', models.DO_NOTHING)
    quantity = models.IntegerField()
    fulfillment = models.ForeignKey(OrderFulfillment, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'order_fulfillmentline'


class OrderOrder(models.Model):
    created = models.DateTimeField()
    tracking_client_id = models.CharField(max_length=36)
    user_email = models.CharField(max_length=254)
    token = models.CharField(unique=True, max_length=36)
    billing_address = models.ForeignKey(AccountAddress, models.DO_NOTHING, blank=True, null=True)
    shipping_address = models.ForeignKey(AccountAddress, models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AccountUser, models.DO_NOTHING, blank=True, null=True)
    total_net = models.DecimalField(max_digits=12, decimal_places=2)
    discount_amount = models.DecimalField(max_digits=12, decimal_places=2)
    discount_name = models.CharField(max_length=255)
    voucher = models.ForeignKey(DiscountVoucher, models.DO_NOTHING, blank=True, null=True)
    language_code = models.CharField(max_length=35)
    shipping_price_gross = models.DecimalField(max_digits=12, decimal_places=2)
    total_gross = models.DecimalField(max_digits=12, decimal_places=2)
    shipping_price_net = models.DecimalField(max_digits=12, decimal_places=2)
    status = models.CharField(max_length=32)
    shipping_method_name = models.CharField(max_length=255, blank=True, null=True)
    shipping_method = models.ForeignKey('ShippingShippingmethod', models.DO_NOTHING, blank=True, null=True)
    display_gross_prices = models.BooleanField()
    translated_discount_name = models.CharField(max_length=255)
    customer_note = models.TextField()
    weight = models.FloatField()
    checkout_token = models.CharField(max_length=36)

    class Meta:
        managed = False
        db_table = 'order_order'


class OrderOrderGiftCards(models.Model):
    order = models.ForeignKey(OrderOrder, models.DO_NOTHING)
    giftcard = models.ForeignKey(GiftcardGiftcard, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'order_order_gift_cards'
        unique_together = (('order', 'giftcard'),)


class OrderOrderevent(models.Model):
    date = models.DateTimeField()
    type = models.CharField(max_length=255)
    order = models.ForeignKey(OrderOrder, models.DO_NOTHING)
    user = models.ForeignKey(AccountUser, models.DO_NOTHING, blank=True, null=True)
    parameters = django.contrib.postgres.fields.JSONField()

    class Meta:
        managed = False
        db_table = 'order_orderevent'


class OrderOrderline(models.Model):
    product_name = models.CharField(max_length=386)
    product_sku = models.CharField(max_length=32)
    quantity = models.IntegerField()
    unit_price_net = models.DecimalField(max_digits=12, decimal_places=2)
    unit_price_gross = models.DecimalField(max_digits=12, decimal_places=2)
    is_shipping_required = models.BooleanField()
    order = models.ForeignKey(OrderOrder, models.DO_NOTHING)
    quantity_fulfilled = models.IntegerField()
    variant = models.ForeignKey('ProductProductvariant', models.DO_NOTHING, blank=True, null=True)
    tax_rate = models.DecimalField(max_digits=5, decimal_places=2)
    translated_product_name = models.CharField(max_length=386)

    class Meta:
        managed = False
        db_table = 'order_orderline'


class PagePage(models.Model):
    slug = models.CharField(unique=True, max_length=100)
    title = models.CharField(max_length=200)
    content = models.TextField()
    created = models.DateTimeField()
    is_published = models.BooleanField()
    publication_date = models.DateField(blank=True, null=True)
    seo_description = models.CharField(max_length=300, blank=True, null=True)
    seo_title = models.CharField(max_length=70, blank=True, null=True)
    content_json = django.contrib.postgres.fields.JSONField()

    class Meta:
        managed = False
        db_table = 'page_page'


class PagePagetranslation(models.Model):
    seo_title = models.CharField(max_length=70, blank=True, null=True)
    seo_description = models.CharField(max_length=300, blank=True, null=True)
    language_code = models.CharField(max_length=10)
    title = models.CharField(max_length=255)
    content = models.TextField()
    page = models.ForeignKey(PagePage, models.DO_NOTHING)
    content_json = django.contrib.postgres.fields.JSONField()

    class Meta:
        managed = False
        db_table = 'page_pagetranslation'
        unique_together = (('language_code', 'page'),)


class PaymentPayment(models.Model):
    gateway = models.CharField(max_length=255)
    is_active = models.BooleanField()
    created = models.DateTimeField()
    modified = models.DateTimeField()
    charge_status = models.CharField(max_length=20)
    billing_first_name = models.CharField(max_length=256)
    billing_last_name = models.CharField(max_length=256)
    billing_company_name = models.CharField(max_length=256)
    billing_address_1 = models.CharField(max_length=256)
    billing_address_2 = models.CharField(max_length=256)
    billing_city = models.CharField(max_length=256)
    billing_city_area = models.CharField(max_length=128)
    billing_postal_code = models.CharField(max_length=256)
    billing_country_code = models.CharField(max_length=2)
    billing_country_area = models.CharField(max_length=256)
    billing_email = models.CharField(max_length=254)
    customer_ip_address = models.GenericIPAddressField(blank=True, null=True)
    cc_brand = models.CharField(max_length=40)
    cc_exp_month = models.IntegerField(blank=True, null=True)
    cc_exp_year = models.IntegerField(blank=True, null=True)
    cc_first_digits = models.CharField(max_length=6)
    cc_last_digits = models.CharField(max_length=4)
    extra_data = models.TextField()
    token = models.CharField(max_length=128)
    currency = models.CharField(max_length=10)
    total = models.DecimalField(max_digits=12, decimal_places=2)
    captured_amount = models.DecimalField(max_digits=12, decimal_places=2)
    checkout = models.ForeignKey(CheckoutCheckout, models.DO_NOTHING, blank=True, null=True)
    order = models.ForeignKey(OrderOrder, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'payment_payment'


class PaymentTransaction(models.Model):
    created = models.DateTimeField()
    token = models.CharField(max_length=128)
    kind = models.CharField(max_length=10)
    is_success = models.BooleanField()
    error = models.CharField(max_length=256, blank=True, null=True)
    currency = models.CharField(max_length=10)
    amount = models.DecimalField(max_digits=12, decimal_places=2)
    gateway_response = django.contrib.postgres.fields.JSONField()
    payment = models.ForeignKey(PaymentPayment, models.DO_NOTHING)
    customer_id = models.CharField(max_length=256, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'payment_transaction'


class ProductAttribute(models.Model):
    slug = models.CharField(max_length=50)
    name = models.CharField(max_length=50)
    product_type = models.ForeignKey('ProductProducttype', models.DO_NOTHING, blank=True, null=True)
    product_variant_type = models.ForeignKey('ProductProducttype', models.DO_NOTHING, blank=True, null=True)
    meta = django.contrib.postgres.fields.JSONField(blank=True, null=True)
    private_meta = django.contrib.postgres.fields.JSONField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'product_attribute'


class ProductAttributetranslation(models.Model):
    language_code = models.CharField(max_length=10)
    name = models.CharField(max_length=100)
    attribute = models.ForeignKey(ProductAttribute, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'product_attributetranslation'
        unique_together = (('language_code', 'attribute'),)


class ProductAttributevalue(models.Model):
    name = models.CharField(max_length=100)
    attribute = models.ForeignKey(ProductAttribute, models.DO_NOTHING)
    slug = models.CharField(max_length=100)
    sort_order = models.IntegerField(blank=True, null=True)
    value = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'product_attributevalue'
        unique_together = (('name', 'attribute'),)


class ProductAttributevaluetranslation(models.Model):
    language_code = models.CharField(max_length=10)
    name = models.CharField(max_length=100)
    attribute_value = models.ForeignKey(ProductAttributevalue, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'product_attributevaluetranslation'
        unique_together = (('language_code', 'attribute_value'),)


class ProductCategory(models.Model):
    name = models.CharField(max_length=128)
    slug = models.CharField(max_length=128)
    description = models.TextField()
    lft = models.IntegerField()
    rght = models.IntegerField()
    tree_id = models.IntegerField()
    level = models.IntegerField()
    parent = models.ForeignKey('self', models.DO_NOTHING, blank=True, null=True)
    background_image = models.CharField(max_length=100, blank=True, null=True)
    seo_description = models.CharField(max_length=300, blank=True, null=True)
    seo_title = models.CharField(max_length=70, blank=True, null=True)
    background_image_alt = models.CharField(max_length=128)
    description_json = django.contrib.postgres.fields.JSONField()
    meta = django.contrib.postgres.fields.JSONField(blank=True, null=True)
    private_meta = django.contrib.postgres.fields.JSONField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'product_category'


class ProductCategorytranslation(models.Model):
    seo_title = models.CharField(max_length=70, blank=True, null=True)
    seo_description = models.CharField(max_length=300, blank=True, null=True)
    language_code = models.CharField(max_length=10)
    name = models.CharField(max_length=128)
    description = models.TextField()
    category = models.ForeignKey(ProductCategory, models.DO_NOTHING)
    description_json = django.contrib.postgres.fields.JSONField()

    class Meta:
        managed = False
        db_table = 'product_categorytranslation'
        unique_together = (('language_code', 'category'),)


class ProductCollection(models.Model):
    name = models.CharField(unique=True, max_length=128)
    slug = models.CharField(max_length=128)
    background_image = models.CharField(max_length=100, blank=True, null=True)
    seo_description = models.CharField(max_length=300, blank=True, null=True)
    seo_title = models.CharField(max_length=70, blank=True, null=True)
    is_published = models.BooleanField()
    description = models.TextField()
    publication_date = models.DateField(blank=True, null=True)
    background_image_alt = models.CharField(max_length=128)
    description_json = django.contrib.postgres.fields.JSONField()
    meta = django.contrib.postgres.fields.JSONField(blank=True, null=True)
    private_meta = django.contrib.postgres.fields.JSONField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'product_collection'


class ProductCollectionproduct(models.Model):
    collection = models.ForeignKey(ProductCollection, models.DO_NOTHING)
    product = models.ForeignKey('ProductProduct', models.DO_NOTHING)
    sort_order = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'product_collectionproduct'


class ProductCollectiontranslation(models.Model):
    seo_title = models.CharField(max_length=70, blank=True, null=True)
    seo_description = models.CharField(max_length=300, blank=True, null=True)
    language_code = models.CharField(max_length=10)
    name = models.CharField(max_length=128)
    collection = models.ForeignKey(ProductCollection, models.DO_NOTHING)
    description = models.TextField()
    description_json = django.contrib.postgres.fields.JSONField()

    class Meta:
        managed = False
        db_table = 'product_collectiontranslation'
        unique_together = (('language_code', 'collection'),)


class ProductDigitalcontent(models.Model):
    use_default_settings = models.BooleanField()
    automatic_fulfillment = models.BooleanField()
    content_type = models.CharField(max_length=128)
    content_file = models.CharField(max_length=100)
    max_downloads = models.IntegerField(blank=True, null=True)
    url_valid_days = models.IntegerField(blank=True, null=True)
    product_variant = models.ForeignKey('ProductProductvariant', models.DO_NOTHING, unique=True)
    meta = django.contrib.postgres.fields.JSONField(blank=True, null=True)
    private_meta = django.contrib.postgres.fields.JSONField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'product_digitalcontent'


class ProductDigitalcontenturl(models.Model):
    token = models.UUIDField(unique=True)
    created = models.DateTimeField()
    download_num = models.IntegerField()
    content = models.ForeignKey(ProductDigitalcontent, models.DO_NOTHING)
    line = models.ForeignKey(OrderOrderline, models.DO_NOTHING, unique=True, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'product_digitalcontenturl'


class ProductProduct(models.Model):
    name = models.CharField(max_length=128)
    description = models.TextField()
    price = models.DecimalField(max_digits=12, decimal_places=2)
    publication_date = models.DateField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    product_type = models.ForeignKey('ProductProducttype', models.DO_NOTHING)
    attributes = models.TextField()  # This field type is a guess.
    is_published = models.BooleanField()
    category = models.ForeignKey(ProductCategory, models.DO_NOTHING)
    seo_description = models.CharField(max_length=300, blank=True, null=True)
    seo_title = models.CharField(max_length=70, blank=True, null=True)
    charge_taxes = models.BooleanField()
    weight = models.FloatField(blank=True, null=True)
    description_json = django.contrib.postgres.fields.JSONField()
    meta = django.contrib.postgres.fields.JSONField(blank=True, null=True)
    private_meta = django.contrib.postgres.fields.JSONField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'product_product'


class ProductProductimage(models.Model):
    image = models.CharField(max_length=100)
    ppoi = models.CharField(max_length=20)
    alt = models.CharField(max_length=128)
    sort_order = models.IntegerField(blank=True, null=True)
    product = models.ForeignKey(ProductProduct, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'product_productimage'


class ProductProducttranslation(models.Model):
    seo_title = models.CharField(max_length=70, blank=True, null=True)
    seo_description = models.CharField(max_length=300, blank=True, null=True)
    language_code = models.CharField(max_length=10)
    name = models.CharField(max_length=128)
    description = models.TextField()
    product = models.ForeignKey(ProductProduct, models.DO_NOTHING)
    description_json = django.contrib.postgres.fields.JSONField()

    class Meta:
        managed = False
        db_table = 'product_producttranslation'
        unique_together = (('language_code', 'product'),)


class ProductProducttype(models.Model):
    name = models.CharField(max_length=128)
    has_variants = models.BooleanField()
    is_shipping_required = models.BooleanField()
    weight = models.FloatField()
    is_digital = models.BooleanField()
    meta = django.contrib.postgres.fields.JSONField(blank=True, null=True)
    private_meta = django.contrib.postgres.fields.JSONField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'product_producttype'


class ProductProductvariant(models.Model):
    sku = models.CharField(unique=True, max_length=32)
    name = models.CharField(max_length=255)
    price_override = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    product = models.ForeignKey(ProductProduct, models.DO_NOTHING)
    attributes = models.TextField()  # This field type is a guess.
    cost_price = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    quantity = models.IntegerField()
    quantity_allocated = models.IntegerField()
    track_inventory = models.BooleanField()
    weight = models.FloatField(blank=True, null=True)
    meta = django.contrib.postgres.fields.JSONField(blank=True, null=True)
    private_meta = django.contrib.postgres.fields.JSONField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'product_productvariant'


class ProductProductvarianttranslation(models.Model):
    language_code = models.CharField(max_length=10)
    name = models.CharField(max_length=255)
    product_variant = models.ForeignKey(ProductProductvariant, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'product_productvarianttranslation'
        unique_together = (('language_code', 'product_variant'),)


class ProductVariantimage(models.Model):
    image = models.ForeignKey(ProductProductimage, models.DO_NOTHING)
    variant = models.ForeignKey(ProductProductvariant, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'product_variantimage'


class ShippingShippingmethod(models.Model):
    name = models.CharField(max_length=100)
    maximum_order_price = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    maximum_order_weight = models.FloatField(blank=True, null=True)
    minimum_order_price = models.DecimalField(max_digits=12, decimal_places=2, blank=True, null=True)
    minimum_order_weight = models.FloatField(blank=True, null=True)
    price = models.DecimalField(max_digits=12, decimal_places=2)
    type = models.CharField(max_length=30)
    shipping_zone = models.ForeignKey('ShippingShippingzone', models.DO_NOTHING)
    meta = django.contrib.postgres.fields.JSONField()

    class Meta:
        managed = False
        db_table = 'shipping_shippingmethod'


class ShippingShippingmethodtranslation(models.Model):
    language_code = models.CharField(max_length=10)
    name = models.CharField(max_length=255, blank=True, null=True)
    shipping_method = models.ForeignKey(ShippingShippingmethod, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'shipping_shippingmethodtranslation'
        unique_together = (('language_code', 'shipping_method'),)


class ShippingShippingzone(models.Model):
    name = models.CharField(max_length=100)
    countries = models.CharField(max_length=749)
    default = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'shipping_shippingzone'


class SiteAuthorizationkey(models.Model):
    name = models.CharField(max_length=20)
    key = models.TextField()
    password = models.TextField()
    site_settings = models.ForeignKey('SiteSitesettings', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'site_authorizationkey'
        unique_together = (('site_settings', 'name'),)


class SiteSitesettings(models.Model):
    header_text = models.CharField(max_length=200)
    description = models.CharField(max_length=500)
    site = models.ForeignKey(DjangoSite, models.DO_NOTHING, unique=True)
    bottom_menu = models.ForeignKey(MenuMenu, models.DO_NOTHING, blank=True, null=True)
    top_menu = models.ForeignKey(MenuMenu, models.DO_NOTHING, blank=True, null=True)
    display_gross_prices = models.BooleanField()
    include_taxes_in_prices = models.BooleanField()
    charge_taxes_on_shipping = models.BooleanField()
    track_inventory_by_default = models.BooleanField()
    homepage_collection = models.ForeignKey(ProductCollection, models.DO_NOTHING, blank=True, null=True)
    default_weight_unit = models.CharField(max_length=10)
    automatic_fulfillment_digital_products = models.BooleanField()
    default_digital_max_downloads = models.IntegerField(blank=True, null=True)
    default_digital_url_valid_days = models.IntegerField(blank=True, null=True)
    company_address = models.ForeignKey(AccountAddress, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'site_sitesettings'


class SiteSitesettingstranslation(models.Model):
    language_code = models.CharField(max_length=10)
    header_text = models.CharField(max_length=200)
    description = models.CharField(max_length=500)
    site_settings = models.ForeignKey(SiteSitesettings, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'site_sitesettingstranslation'
        unique_together = (('language_code', 'site_settings'),)


class SocialAuthAssociation(models.Model):
    server_url = models.CharField(max_length=255)
    handle = models.CharField(max_length=255)
    secret = models.CharField(max_length=255)
    issued = models.IntegerField()
    lifetime = models.IntegerField()
    assoc_type = models.CharField(max_length=64)

    class Meta:
        managed = False
        db_table = 'social_auth_association'
        unique_together = (('server_url', 'handle'),)


class SocialAuthCode(models.Model):
    email = models.CharField(max_length=254)
    code = models.CharField(max_length=32)
    verified = models.BooleanField()
    timestamp = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'social_auth_code'
        unique_together = (('email', 'code'),)


class SocialAuthNonce(models.Model):
    server_url = models.CharField(max_length=255)
    timestamp = models.IntegerField()
    salt = models.CharField(max_length=65)

    class Meta:
        managed = False
        db_table = 'social_auth_nonce'
        unique_together = (('server_url', 'timestamp', 'salt'),)


class SocialAuthPartial(models.Model):
    token = models.CharField(max_length=32)
    next_step = models.SmallIntegerField()
    backend = models.CharField(max_length=32)
    data = models.TextField()
    timestamp = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'social_auth_partial'


class SocialAuthUsersocialauth(models.Model):
    provider = models.CharField(max_length=32)
    uid = models.CharField(max_length=255)
    extra_data = models.TextField()
    user = models.ForeignKey(AccountUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'social_auth_usersocialauth'
        unique_together = (('provider', 'uid'),)
